terraform {
  backend "s3" {
    bucket         = "shravan29042021"
    key            = "shravan/20210416/terraform.tfstate"
    region         = "us-east-1"
    encrypt        = true
  }
}